package com.software.central.fantasy_football;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class MainActivity extends ActionBarActivity {

    public ProgressBar progressBar;
    public TextView responseView;
    public EditText emailText;
    public Bitmap bitmap;
    ImageView i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        responseView = (TextView) findViewById(R.id.responseView);
        emailText = (EditText) findViewById(R.id.emailText);
        new GetImages().execute();
        i = (ImageView)findViewById(R.id.imageView);


    }

    public void searchEmail(View view){
        new RetrieveFeedTask().execute();
    }

    /*********************************************************************************************
     *     ASYNC 1
     *     ***************************************************************************************
     */
    class RetrieveFeedTask extends AsyncTask<Void, Void, String> {

        private Exception exception;
        protected String email;

        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            responseView.setText("");
            email = emailText.getText().toString();
            email = email.replaceAll(" ", "%20");
        }

        protected String doInBackground(Void... urls) {

            // Do some validation here
            JSONObject jsonResponse;
            JSONObject json2;
            String display, line1, line2 = "", line3 = "", line4 = "";

            try {
                //URL url = new URL("https://api.fullcontact.com/v2/person.json?" + "email=" + email + "&apiKey=" + "dcbc4a79c5da9f63");
                URL url = new URL("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"+email+"%2C%20mi%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    String response =  stringBuilder.toString();
                    try {
                        /****** Creates a new JSONObject with name/value mappings from the JSON string. ********/
                        jsonResponse = new JSONObject(response);
                        json2 = jsonResponse.getJSONObject("query").getJSONObject("results").getJSONObject("channel");
                        line1 = (String) json2.get("title");
                        line2 = (String) json2.getJSONObject("forcast").get("temp");
                        line2 = line2 +" Degrees" ;
                        display = line1 + "\n" + line2 + "\n" + line3 + "\n" + line4;

                    } catch (JSONException e) {
                        Log.e("ERROR", e.getMessage(), e);
                        display = "THERE WAS AN ERROR";
                    }

                    return display;
                }
                finally{
                    urlConnection.disconnect();
                }
            }
            catch(Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {
            String display;


            if(response == null) {
                display = "THERE WAS AN ERROR";

            }
            else{
                display = response;
            }
            progressBar.setVisibility(View.GONE);
            responseView.setText(display);
            Log.i("INFO", response);



        }
    }

    /**********************************************************************************************
     *   ASYNC 2
     *   ******************************************************************************************
     */
    class GetImages extends AsyncTask<Void, Void, String> {



        protected void onPreExecute() {

        }

        protected String doInBackground(Void... urls) {

            // Do some validation here
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)
                        new URL("http://l.yimg.com/a/i/brand/purplelogo//uh/us/news-wea.gif").getContent());
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return "";
        }

        protected void onPostExecute(String response) {

        i.setImageBitmap(bitmap);
        }
    }
}
